﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI_LoPy.Models.DataClasses
{
    /// <summary>
    /// Value class to hold all values
    /// </summary>
    public class Value
    {
        public uint timestamp { get; set; }
        public double light_intensity { get; set; }
        public double air_pressure { get; set; }
        public double temperature { get; set; }

        public Value(uint timestamp, double light_intensity, double air_pressure, double temperature)
        {
            this.timestamp = timestamp;
            this.light_intensity = light_intensity;
            this.air_pressure = air_pressure;
            this.temperature = temperature;
        }
    }
}
