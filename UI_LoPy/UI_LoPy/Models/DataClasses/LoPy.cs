﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI_LoPy.Models.Classes
{
    /// <summary>
    /// LoPy class to display a LoPy
    /// </summary>
    public class LoPy
    {
        int id;
        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        string name;
        string Name
        {
            get { return name; }
            set { name = value; }
        }

    }
}
