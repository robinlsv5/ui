﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI_LoPy.Models.DataClasses
{
    public delegate void OnLogLineAppended(string logtext);

    public class Log
    {
        public static string TimeString { get { return DateTime.Now.ToShortTimeString(); } }

        private static OnLogLineAppended LogLineAppended;

        private static FileInfo LogFile { get; set; }

        /// <summary>
        /// Initializes the log and creates the event and folders.
        /// </summary>
        /// <param name="appendTarget">Append it to the method given</param>
        public static void Initialize(Action<string> appendTarget = null)
        {
            DirectoryInfo dir = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LoPy\\Logs");
            if (appendTarget != null)
            {
                LogLineAppended += new OnLogLineAppended(appendTarget);
            }

            LogLineAppended += new DataClasses.OnLogLineAppended(OnLogLineAppended);

            if(Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LoPy\\Logs"))
            {
                if(!dir.GetFiles().Any())
                {
                    using (FileStream fs = File.Create(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LoPy\\Logs\\LopyUI_Log" + DateTime.Now.ToShortDateString() + ".log"))
                    {
                        fs.Close();
                    }
                }
                //Loop through all files to see existing log file of today.
                foreach(FileInfo file in dir.GetFiles())
                {
                    file.Refresh();
                    if(file.CreationTime.Date == DateTime.Today)
                    {
                        LogFile = file;
                    }
                    else
                    {
                        LogFile = new FileInfo(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LoPy\\Logs\\LoPyUI_Log" + DateTime.Now.ToShortDateString() + ".log");  
                    }
                }
            }
            else
            {
                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LoPy\\Logs");
                Initialize(); //Run again to do the same structure
            }
        }

        /// <summary>
        /// Appends a new log line to the method in the event
        /// </summary>
        /// <param name="text"></param>
        public static void AppendLogLine(string text)
        {
            string finalText = string.Format("{0} : {1} {2}", DateTime.Now.ToLongTimeString(), text, Environment.NewLine);

            LogLineAppended(finalText);
        }

        /// <summary>
        /// Write to the log file
        /// </summary>
        /// <param name="logtext">Text to log</param>
        private static void OnLogLineAppended(string logtext)
        {
            if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LoPy\\Logs"))
            {
                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LoPy\\Logs");
            }

            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LoPy\\Logs\\LoPyUI_Log" + DateTime.Now.ToShortDateString() + ".log"))
            {

                string filename = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LoPy\\Logs\\LoPyUI_Log" + DateTime.Now.ToShortDateString() + ".log";

                using (FileStream fs =
                   File.Open(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\LoPy\\Logs\\LoPyUI_Log" + DateTime.Now.ToShortDateString() + ".log", FileMode.Append))
                {
                    byte[] txt = new UTF8Encoding(true).GetBytes(logtext);
                    fs.Write(txt, 0, txt.Length);
                    fs.Dispose();
                }
            }
        }
    }
}
