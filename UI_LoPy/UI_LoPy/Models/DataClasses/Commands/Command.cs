﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UI_LoPy.Models.DataClasses.Commands.Requests;
using UI_LoPy.Models.TCPConnection;

namespace UI_LoPy.Models.DataClasses.Commands
{
    public class Command
    {
        /// <summary>
        /// The name of the command (WIP)
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// The request itself
        /// </summary>
        public Request RequestCommand
        {
            get;
            set;
        }

        /// <summary>
        /// The request command in JSON Format
        /// </summary>
        public string JSONFormat
        {
            get;
            set;
        }

        /// <summary>
        /// Executes the current command and sends it to the server
        /// </summary>
        /// <returns></returns>
        public bool Execute()
        {
            this.JSONFormat = ToJson();

            if (!JSONFormat.Equals(string.Empty))
            {
                return TCPRequest.SendToServer(this);
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Converts the current RequestCommand to JSON
        /// </summary>
        /// <returns>Json formatted command</returns>
        private string ToJson()
        {
            string json = JsonConvert.SerializeObject(this.RequestCommand);
            return json;
        }
    }
}
