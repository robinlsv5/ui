﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI_LoPy.Models.DataClasses.Commands.Requests
{
    /// <summary>
    /// Type of the request, can be added according to protocol
    /// </summary>
    public enum RequestType
    {
        Devices,
        Values
    }
}
