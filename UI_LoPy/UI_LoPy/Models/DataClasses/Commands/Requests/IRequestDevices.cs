﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI_LoPy.Models.DataClasses.Commands.Requests
{
    /// <summary>
    /// Request interface to request devices.
    /// </summary>
    public interface IRequestDevices
    {
        string RequestType { get; set; }

        int id { get; set; }

        string name { get; set; }
    }
}
