﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI_LoPy.Models.DataClasses.Commands.Requests
{
    /// <summary>
    /// Interface for the values request. Convert to this.
    /// </summary>
    public interface IRequestValues
    {
        string RequestType { get; set; }

        int id { get; set; }

        string name { get; set; }

        List<Value> values { get; set; }
    }
}
