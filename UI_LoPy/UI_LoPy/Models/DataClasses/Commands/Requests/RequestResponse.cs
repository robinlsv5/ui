﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI_LoPy.Models.DataClasses.Commands.Requests
{
    /// <summary>
    /// Response from the server, includes interfaces to control which request is returned.
    /// </summary>
    public class RequestResponse : IRequestDevices, IRequestValues
    {
        public string RequestType { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public string name { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public List<Value> values { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }
}
