﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.Serialization;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace UI_LoPy.Models.DataClasses.Commands.Requests
{
    public class Request
    {
        [JsonIgnore]
        public RequestType RequestType_ { get; set; }

        public string RequestType {
            get { return Enum.GetName(typeof(RequestType), RequestType_); } }
                
        public int Id { get; set; }

        public long TimeStamp { get; set; }
    }
}
