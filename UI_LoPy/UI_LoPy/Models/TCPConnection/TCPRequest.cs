﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using UI_LoPy.Models.DataClasses;
using UI_LoPy.Models.DataClasses.Commands;

namespace UI_LoPy.Models.TCPConnection
{
    public static class TCPRequest
    {
        private static TcpClient tcpClient { get; set; }


        /// <summary>
        /// Initializes the TCP connection to the serverr
        /// </summary>
        /// <param name="ip">IP to connect to</param>
        /// <param name="port"> Port to connect to</param>
        /// <returns></returns>
        public static bool InitializeConnection(IPAddress ip, int port)
        {
            try
            {
                tcpClient = new TcpClient(ip.ToString(), port);

                if (tcpClient.Connected)
                {
                    Log.AppendLogLine("[SUCCESS] Succesfully connected to server on: " + ip.ToString());
                    return true;
                }
            }
            catch (SocketException ex)
            {
                Log.AppendLogLine("[FAIL] A problem occurred while setting up the TcpClient. " + ex.Message);
                return false;
            }
            return false;
        }

        /// <summary>
        /// Sends command to the server.
        /// </summary>
        /// <param name="command">Command to send</param>
        /// <returns>true/false if succeeded.</returns>
        public static bool SendToServer(Command command)
        {
            int amountErrors = 0;

            if (tcpClient != null)
            {
                try
                {
                    Log.AppendLogLine("[START] Attempting to send data to server.. Please wait..");
                    if (tcpClient.Connected)
                    {
                        try
                        {
                            string msg = command.JSONFormat;
                            byte[] bytesSend = ASCIIEncoding.ASCII.GetBytes(msg);

                            NetworkStream nwStream = tcpClient.GetStream();
                            nwStream.Write(bytesSend, 0, bytesSend.Length);

                            Log.AppendLogLine("[SUCCESS] Successfully sent request to server");
                            return true;
                        }
                        catch (EncoderFallbackException eex)
                        {
                            Log.AppendLogLine("[FAIL] Message Encoder failed. Error: " + eex.Message);
                            amountErrors++;
                        }
                        catch (IOException ex)
                        {
                            Log.AppendLogLine("[FAIL] Failed to send bytes through networkstream. Error: " + ex.Message);
                            amountErrors++;
                        }

                    }
                    else
                    {
                        Log.AppendLogLine("[FAIL] Unable to send to server. Client not connected to server.");
                    }
                }
                finally
                {
                    Log.AppendLogLine("[DONE] Finished trying to send data to server with: " + amountErrors + " errors.");

                }

            }
            else
            {
                Log.AppendLogLine("[FAIL] Failed to send data to server, TcpClient unconnected");
            }

            if (amountErrors > 0)
                return false;
            else
                return true;
        }
    }
}
