﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using UI_LoPy.Models.Classes;
using UI_LoPy.Models.DataClasses;
using UI_LoPy.Models.DataClasses.Commands;
using UI_LoPy.Models.DataClasses.Commands.Requests;
using UI_LoPy.Models.Enumerations;
using UI_LoPy.Models.TCPConnection;

namespace UI_LoPy.Models
{
    public class MainModel
    {
        public MainModel()
        {

        }

        /// <summary>
        /// Retrieves all app.config appsettings entries
        /// </summary>
        /// <returns>List of key value pairs</returns>
        public Dictionary<string, string> GetAllAppSettings()
        {
            Dictionary<string, string> returnValue = new Dictionary<string, string>();

            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            foreach (KeyValueConfigurationElement key in config.AppSettings.Settings)
            {
                returnValue.Add(key.Key, key.Value);
            }

            return returnValue;
        }

        /// <summary>
        /// Function connects to the server and returns a connection status.
        /// </summary>
        /// <returns></returns>
        public ConnectionStatus ConnectToServer()
        {
            IPAddress ip = IPAddress.None;
            int port = 8080;
            foreach(KeyValuePair<string, string> key in GetAllAppSettings())
            {
                switch(key.Key)
                {
                    case "ipaddress":
                        if(!IPAddress.TryParse(key.Value, out ip))
                            Log.AppendLogLine("[ERROR] String to IPAddress conversion failed in 'ConnectToServer' value: '" + key.Value + "'");
                        break;
                    case "port":
                        if (!int.TryParse(key.Value, out port))
                            Log.AppendLogLine("[ERROR] String to Int conversion failed in 'ConnectToServer'. value: '" + key.Value + "'" );
                        break;
                }
            }

            TCPRequest.InitializeConnection(ip, port);

            Request request = new Request();
            request.RequestType_ = RequestType.Devices;
            request.TimeStamp = 0;
            request.Id = 0;

            Command cmd = new Command();
            cmd.RequestCommand = request;
            cmd.Name = "Temperature";

            cmd.Execute();

            return ConnectionStatus.Connected;
        }

        public List<LoPy> RetrieveLoPyData()
        {
            return null;
        }
    }
}
