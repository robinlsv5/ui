﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UI_LoPy.Models.DataClasses;

namespace UI_LoPy.Models
{
    public class SettingsModel
    {
        /// <summary>
        /// Saves keys to the config
        /// </summary>
        /// <param name="keys">Dictionary to add</param>
        public void SaveToConfig(Dictionary<string, string> keys)
        {
            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            string logKeys = "";
            foreach (KeyValuePair<string, string> key in keys)
            {
                logKeys += "[Key: " + key.Key + ", Value: " + key.Value + "], ";
            }

            Log.AppendLogLine("[START] Saving " + logKeys + " to the appconfig");

            foreach (KeyValuePair<string, string> key in keys)
            {
                if (!ConfigEntryExist(key.Key))
                {
                    config.AppSettings.Settings.Add(key.Key, key.Value);
                }
                else
                {
                    UpdateExistingKey(key.Key, key.Value);
                }
            }

            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
            Log.AppendLogLine("[DONE] Saved to app config");
        }

        /// <summary>
        /// Updates existing key in config
        /// </summary>
        /// <param name="key"></param>
        /// <param name="newvalue"></param>
        public void UpdateExistingKey(string key, string newvalue)
        {
            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            for (int i = 0; i < config.AppSettings.Settings.Count; i++)
            {
                if (config.AppSettings.Settings.AllKeys[i] == key)
                {
                    config.AppSettings.Settings.Remove(key);
                    config.AppSettings.Settings.Add(key, newvalue);
                }
            }
        }

        /// <summary>
        /// Retrieves all app.config appsettings entries
        /// </summary>
        /// <returns>List of key value pairs</returns>
        public Dictionary<string, string> GetAllAppSettings()
        {
            Dictionary<string, string> returnValue = new Dictionary<string, string>();

            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            foreach (KeyValueConfigurationElement key in config.AppSettings.Settings)
            {
                returnValue.Add(key.Key, key.Value);
            }

            return returnValue;
        }

        /// <summary>
        /// Checks if entry exists
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool ConfigEntryExist(string key)
        {
            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            foreach (KeyValueConfigurationElement k in config.AppSettings.Settings)
            {
                if (k.Key == key)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
