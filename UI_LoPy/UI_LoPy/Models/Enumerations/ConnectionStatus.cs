﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UI_LoPy.Models.Enumerations
{
    public enum ConnectionStatus
    {
        Connecting,
        Connected,
        Not_Connected,
        Connection_Error
    }
}
