﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UI_LoPy.Models;
using UI_LoPy.Models.DataClasses;
using UI_LoPy.Models.Enumerations;
using UI_LoPy.MVVMResources;

namespace UI_LoPy.ViewModel
{
    public class MainViewModel : ObservableObject
    {
        public MainModel mainModel;

        private string logtext;
        public string LogText
        {
            get { return logtext; }
            set
            {
                logtext = value;
                RaisePropertyChangedEvent("LogText");
            }
        }

        #region RelayCommands
        public RelayCommand addDeviceCommand { get; set; }
        public RelayCommand settingsCommand { get; set; }
        public RelayCommand graphCommand { get; set; }
        #endregion

        #region Status Text
        private ConnectionStatus connectionStatus { get; set; }
        public string statusTextConnected
        {
            get
            {
                return Enum.GetName(typeof(ConnectionStatus), connectionStatus);
            }
        }
        #endregion

        private ObservableObject currentViewModel;
        public ObservableObject CurrentViewModel
        {
            get { return currentViewModel; }
            set
            {
                currentViewModel = value;
                RaisePropertyChangedEvent("CurrentViewModel");
            }
        }
        public MainViewModel()
        {
            Log.Initialize(LogCallBack);
            Log.AppendLogLine("[STARTING UP] ===== LoPy GUI is starting up.. =====");
            this.mainModel = new MainModel();

            this.connectionStatus = mainModel.ConnectToServer();
            Log.AppendLogLine("[START UP COMPLETE] ====== LoPy GUI has been started succesfully =====");
        }

        private void LogCallBack(string text)
        {
            LogText += text;
        }

    }
}

