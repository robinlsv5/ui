﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using UI_LoPy.Models;
using UI_LoPy.Models.DataClasses;
using UI_LoPy.Models.TCPConnection;
using UI_LoPy.MVVMResources;

namespace UI_LoPy.ViewModel
{
    public class SettingsViewModel : ObservableObject
    {
        private SettingsModel model;

        public RelayCommand CommandConnect { get; set; }

        private string connectionipAddress;
        /// <summary>
        /// Connection IP text input (UI)
        /// </summary>
        public string connectionIPAddress
        {
            get { return connectionipAddress; }
            set
            {
                connectionipAddress = value;
                RaisePropertyChangedEvent("connectionIPAddress");
            }
        }

        private string connectionport;
        /// <summary>
        /// Connection Port text input       (UI)
        /// </summary>
        public string connectionPort
        {
            get { return connectionport; }
            set
            {
                connectionport = value;
                RaisePropertyChangedEvent("connectionPort");
            }
        }

        /// <summary>
        /// Settings view model constructor
        /// </summary>
        public SettingsViewModel()
        {
            model = new SettingsModel();
            FillFromAppSettings();

            CommandConnect = new RelayCommand(x=>Connect());
        }

        /// <summary>
        /// Fill all values from app.config.
        /// </summary>
        private void FillFromAppSettings()
        {
            foreach(KeyValuePair<string, string> keypair in model.GetAllAppSettings())
            {
                switch(keypair.Key)
                {
                    case "ipaddress":
                        connectionIPAddress = keypair.Value;
                        break;
                    case "port":
                        connectionPort = keypair.Value;
                        break;
                }
            }
        }

        #region Relay methods

        /// <summary>
        /// Connects to the server and saves to app.config
        /// </summary>
        private void Connect()
        {
            IPAddress ipParsed;
            int port = 8080;

            if(IPAddress.TryParse(connectionipAddress, out ipParsed))
            {
                if(!int.TryParse(connectionPort, out port))
                {
                    Log.AppendLogLine("[ERROR] Failed to convert: '" + connectionPort + "' to integer!");
                }

                Dictionary<string, string> dictionary = new Dictionary<string, string>();
                dictionary.Add("ipaddress", ipParsed.ToString());
                dictionary.Add("port", connectionPort);

                model.SaveToConfig(dictionary);
            }

            TCPRequest.InitializeConnection(ipParsed, port);
        }

        #endregion
    }
}
