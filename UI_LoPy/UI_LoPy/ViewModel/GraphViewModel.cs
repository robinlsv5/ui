﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using UI_LoPy.Models;
using UI_LoPy.Models.TCPConnection;
using UI_LoPy.MVVMResources;

namespace UI_LoPy.ViewModel
{
    /// <summary>
    /// View model for the Graphs 
    /// </summary>
    public class GraphViewModel : ObservableObject
    {
        private GraphModel model;
        private ObservableObject graphSelected;
        /// <summary>
        /// Controls which graph is displayed.
        /// </summary>
        public ObservableObject GraphSelected
        {
            get { return graphSelected; }
            set { graphSelected = value; RaisePropertyChangedEvent("GraphSelected"); }
        }

        public GraphViewModel()
        {
            GraphSelected = new TemperatureGraphViewModel();
        }
    }
}
