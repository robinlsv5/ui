﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using UI_LoPy.ViewModel;

namespace UI_LoPy
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            MainWindow mw = new UI_LoPy.MainWindow();
            mw.DataContext = new MainViewModel();
            mw.Show();
        }
    }
}
