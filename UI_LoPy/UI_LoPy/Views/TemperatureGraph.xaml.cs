﻿using LiveCharts;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using UI_LoPy.ViewModel;

namespace UI_LoPy.Views
{
    /// <summary>
    /// Interaction logic for TemperatureGraph.xaml
    /// </summary>
    public partial class TemperatureGraph : UserControl
    {
        public TemperatureGraph()
        {
            InitializeComponent();
            this.DataContext = new TemperatureGraphViewModel();


            SeriesCollection = new SeriesCollection
            {
                new LineSeries
                {
                    Title = "Temperature",
                    Values = new ChartValues<double> { 21, 22, 23, 20 , 19 }
                }
            };

            Labels = new[] { "12:00", "14:00", "16:00", "18:00", "20:00" };
            YFormatter = value => (value.ToString("0.0") + "°C");

            //modifying any series values will also animate and update the chart
            SeriesCollection[0].Values.Add(5d);

            DataContext = this;
        }

        public SeriesCollection SeriesCollection { get; set; }
        public string[] Labels { get; set; }
        public Func<double, string> YFormatter { get; set; }
    }
}
